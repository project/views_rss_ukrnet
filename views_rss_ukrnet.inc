<?php

function views_rss_ukrnet_preprocess_item_ukrnet_full_text(&$variables) {
  if (empty($variables['elements'][0]['value'])) {
    return;
  }

  $key = $variables['elements'][0]['key'];
  if('ukrnet:full-text' == $key) {
    $variables['elements'][0]['key'] = 'full-text';
    $variables['elements'][0]['value'] = "\n<![CDATA[\n" . $variables['elements'][0]['value'] . "\n]]>\n";
  }
}
